package parser

import (
	"fmt"
	//"io"
	"sql_parser/ast"
	"sql_parser/scanner"
	"sql_parser/token"
)

// Parser represents a parser.
type Parser struct {
	scanner *scanner.Scanner
	/*buf     struct {
		tok token.Token // last read token
		lit string      // last read literal
		n   int         // buffer size (max=1)
	}*/

	tok token.Token
	lit string

	Errors scanner.ErrorList
	// Next token
	pos token.Pos // token position
	//tok token.Token // one token look-ahead
	//lit string      // token literal
}

// NewParser returns a new instance of Parser.
/*func NewParser(r io.Reader) *Parser {
	return &Parser{s: NewScanner(r)}
}*/
func NewParser(src []byte) (p *Parser) {
	p = &Parser{scanner: scanner.NewScanner(src)}
	p.next()
	return
}

/*func (p *Parser) init() {
	fmt.Printf("INIT")
	p.next()
}*/

// Parse parses a SQL SELECT statement.
func (p *Parser) Parse() (*ast.SelectStatement, error) {
	stmt := &ast.SelectStatement{}

	// First token should be a "SELECT" keyword.
	p.expect(token.SELECT)

	// Next we should loop over all our comma-delimited fields.
	if p.tok == token.MUL || p.tok == token.IDENT {
		stmt.Fields = append(stmt.Fields, p.lit)
	}
	p.next()
	for p.tok == token.COMMA {
		p.next()
		stmt.Fields = append(stmt.Fields, p.expect(token.IDENT))
	}
	p.expect(token.FROM)

	// Finally we should read the table name.
	stmt.TableName = p.expect(token.IDENT)
	// Return the successfully parsed statement.
	return stmt, nil
}

func (p *Parser) parseParam() (param ast.Param) {
	param = ast.Param{}
	param.Name = p.expect(token.IDENT)
	param.DataType = ast.BasicDataType{Name: p.expect(token.IDENT)}
	return
}

func (p *Parser) parseParameterList() (params []ast.Param) {
	params = []ast.Param{}
	p.expect(token.LPAREN)
	if p.tok == token.IDENT {
		params = append(params, p.parseParam())
		for p.tok == token.COMMA {
			p.next()
			params = append(params, p.parseParam())
		}
		//p.next()
	}
	p.expect(token.RPAREN)
	return
}

func (p *Parser) parseFunction() (*ast.FunctionDecl, error) {
	//p.next()
	stmt := &ast.FunctionDecl{}
	p.expect(token.FUNCTION)
	stmt.Name = p.expect(token.IDENT)
	stmt.Params = p.parseParameterList()
	p.expect(token.RETURN)
	stmt.ReturnType = p.expect(token.IDENT)
	p.expect(token.SEMICOLON)

	return stmt, nil
}

func (p *Parser) error(pos token.Pos, msg string) {
	p.Errors.Add(p.pos, msg)
}

func (p *Parser) expect(tok token.Token) (lit string) {
	lit = p.lit
	if p.tok != tok {
		p.error(1, fmt.Sprintf("found (%s,%s) expected %s", p.tok, p.lit, tok))
	}
	p.next()
	return
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
/*func (p *Parser) scan() (tok token.Token, lit string) {
	// If we have a token on the buffer, then return it.
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}

	// Otherwise read the next token from the scanner.
	tok, lit = p.scanner.Scan()

	// Save it to the buffer in case we unscan later.
	p.buf.tok, p.buf.lit = tok, lit

	return
}*/

func (p *Parser) next() {
	p.pos, p.tok, p.lit = p.scanner.Scan()
}

// scanIgnoreWhitespace scans the next non-whitespace token.
/*func (p *Parser) scanIgnoreWhitespace() (tok token.Token, lit string) {
	if tok, lit = p.scan(); tok == token.WS {
		tok, lit = p.scan()
	}
	return
}*/

// unscan pushes the previously read token back onto the buffer.
//func (p *Parser) unscan() { p.buf.n = 1 }
