package parser

import (
	"reflect"
	//"strings"
	"testing"
	//"bytes"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	//"sql_parser"
	"sql_parser/ast"
	"sql_parser/scanner"
	//"sql_parser/token"
)

// Ensure the parser can parse strings into Statement ASTs.
func TestParser_ParseStatement(t *testing.T) {
	Convey("Проверка парсера", t, func() {
		var tests = []struct {
			s    string
			stmt *ast.SelectStatement
			err  string
		}{
			// Single field statement
			{
				s: `SELECT name FROM tbl`,
				stmt: &ast.SelectStatement{
					Fields:    []string{"name"},
					TableName: "tbl",
				},
			},

			// Multi-field statement
			{
				s: `SELECT first_name, last_name, age FROM my_table`,
				stmt: &ast.SelectStatement{
					Fields:    []string{"first_name", "last_name", "age"},
					TableName: "my_table",
				},
			},

			// Select all statement
			{
				s: `SELECT * FROM my_table`,
				stmt: &ast.SelectStatement{
					Fields:    []string{"*"},
					TableName: "my_table",
				},
			},

			// Errors
			{s: `foo`, err: `found "foo", expected SELECT`},
			{s: `SELECT !`, err: `found "!", expected field`},
			{s: `SELECT field xxx`, err: `found "xxx", expected FROM`},
			{s: `SELECT field FROM *`, err: `found "*", expected table name`},
		}

		for i, tt := range tests {
			//stmt, err := NewParser(strings.NewReader(tt.s)).Parse()
			p := NewParser([]byte(tt.s))
			stmt, _ := p.Parse()
			//for _, pr := range p.Errors {
			//	fmt.Printf("ERR-%s=%s\n", tt.s, pr.Msg)
			//}

			Convey(fmt.Sprintf("Проверим выражение %d,%q,table_name=%s,fields=%s", i, tt.s, stmt.TableName, stmt.Fields), func() {
				if stmt != nil && tt.stmt != nil {
					/*fmt.Printf("SQL-%s\n", tt.s)
					fmt.Printf("1stmt=%q,%q", stmt.TableName, stmt.Fields)
					fmt.Printf("2stmt=%q,%q", tt.stmt.TableName, tt.stmt.Fields)
					*/

					So(reflect.DeepEqual(stmt, tt.stmt), ShouldBeTrue)

				} else {
					//So(reflect.DeepEqual(tt.err, errstring(err)), ShouldBeTrue)
					So(tt.err == "" && !reflect.DeepEqual(tt.stmt, stmt), ShouldBeFalse)
				}
			})
		}
	})

	Convey("Проверка парсинга функции", t, func() {
		var tests = []struct {
			s         string
			func_decl *ast.FunctionDecl
			err       scanner.ErrorList
		}{

			{s: "function test()return clob;",
				func_decl: &ast.FunctionDecl{
					Name:       "test",
					Params:     []ast.Param{},
					ReturnType: "clob"},
			},

			{s: "function test(a varchar2)return clob;",
				func_decl: &ast.FunctionDecl{
					Name: "test",
					Params: []ast.Param{
						{"a", ast.BasicDataType{Name: "varchar2"}}},
					ReturnType: "clob",
				},
			},
			{s: "function test(a varchar2,b varchar2)return clob;",
				func_decl: &ast.FunctionDecl{
					Name: "test",
					Params: []ast.Param{
						{"a", ast.BasicDataType{Name: "varchar2"}},
						{"b", ast.BasicDataType{Name: "varchar2"}}},
					ReturnType: "clob",
				},
			},
			/*{s: "function test;(a varchar2,b varchar2)return clob;",
				err: scanner.ErrorList{scanner.Error{13, "found (;,;) expected ("},
					scanner.Error{Pos: 14, Msg: "found ((,() expected )"},
					scanner.Error{Pos: 15, Msg: "found (IDENT,a) expected RETURN"},
					scanner.Error{Pos: 25, Msg: "found (,,,) expected ;"}},
			},*/
		}

		for i, tt := range tests {
			p := NewParser([]byte(tt.s))
			funcDecl, _ := p.parseFunction()
			Convey(fmt.Sprintf("%d. Проверим полное совпадение функций %s", i, tt.s), func() {
				So(p.Errors, ShouldResemble, tt.err)
				So(funcDecl, ShouldResemble, tt.func_decl)
			})
			/*Convey(fmt.Sprintf("%d. Проверим функцию %q", i, tt.s), func() {
				Convey(fmt.Sprintf("Проверим имя функции"), func() {
					So(funcDecl.Name, ShouldEqual, tt.func_decl.Name)
				})
				Convey(fmt.Sprintf("Проверим параметры"), func() {
					for i := range tt.func_decl.Params {
						Convey(fmt.Sprintf("%d. Проверим параметр", i), func() {
							Convey(fmt.Sprintf("Проверим имя параметра"), func() {
								So(funcDecl.Params[i].Name, ShouldEqual, tt.func_decl.Params[i].Name)
							})
							Convey(fmt.Sprintf("Проверим тип данных параметра"), func() {
								So(fmt.Sprintf("%s", funcDecl.Params[i].DataType), ShouldEqual, fmt.Sprintf("%s", tt.func_decl.Params[i].DataType))
							})
						})
					}
				})
				Convey(fmt.Sprintf("Проверим полное совпадение функций"), func() {
					So(funcDecl, ShouldResemble, tt.func_decl)
				})
			})*/
		}
	})

	Convey("Проверка парсера ошибочной функции", t, func() {
		var tests = []struct {
			s   string
			err string
		}{
			{"function test;(a varchar2)return clob;", `found (;,;) expected (`},
		}
		for i, tt := range tests {
			p := NewParser([]byte(tt.s))
			p.parseFunction()
			Convey(fmt.Sprintf(`Сравним ошибки для выражения %d: "%s" "%s" и "%s"`, i, tt.s, tt.err, p.Errors[0].Msg), func() {
				//So(stmt, ShouldBeNil)
				So(tt.err, ShouldEqual, p.Errors[0].Msg)
			})
		}
	})

}

// errstring returns the string representation of an error.
func errstring(err error) string {
	if err != nil {
		return err.Error()
	}
	return ""
}
