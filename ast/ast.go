package ast

import (
//"sql_parser/token"
//"strings"
)

// SelectStatement represents a SQL SELECT statement.
type SelectStatement struct {
	Fields    []string
	TableName string
}

type DataTypeInterface interface {
	String() string
}

type BasicTypeEnum int

const (
	INTEGER = iota
	NUMBER
	VARCHAR
	CLOB
)

type BasicDataType struct {
	DataTypeInterface
	Name     string
	DataType BasicTypeEnum
	Digit1   int
	Digit2   int
}

type Param struct {
	Name     string
	DataType DataTypeInterface
}

type FunctionDecl struct {
	Name       string
	Params     []Param
	ReturnType string
}

/*func (bdt BasicDataType) String() string {
	return bdt.Name
}*/

/*func (f *FunctionDecl) String() string {
	return "function " + f.Name
}
*/
