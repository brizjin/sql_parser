package scanner

import (
	//"fmt"
	"sql_parser/token"
)

type Error struct {
	Pos token.Pos
	Msg string
}

type ErrorList []Error

func (p *ErrorList) Add(pos token.Pos, msg string) {
	*p = append(*p, Error{pos, msg})
}
