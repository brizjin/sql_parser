package scanner

import (
	//"bufio"
	//"bytes"
	//"io"
	"sql_parser/token"
	"strings"
	"unicode/utf8"
)

// eof represents a marker rune for the end of the reader.
var eof = rune(0)

// Scanner represents a lexical scanner.
type Scanner struct {
	//r *bufio.Reader
	src []byte // source

	ch         rune // current character
	offset     int  // character offset
	rdOffset   int  // reading offset (position after current character)
	lineOffset int  // current line offset
}

// NewScanner returns a new instance of Scanner.
func NewScanner(src []byte) *Scanner {
	//return &Scanner{r: bufio.NewReader(r)}
	s := &Scanner{}
	s.src = src
	s.ch = ' '
	s.offset = 0
	s.rdOffset = 0
	s.lineOffset = 0
	return s
}

// Scan returns the next token and literal value.
func (s *Scanner) Scan() (pos token.Pos, tok token.Token, lit string) {
	s.skipWhitespace()

	pos = token.Pos(s.offset)
	// If we see whitespace then consume all contiguous whitespace.
	// If we see a letter then consume as an ident or reserved word.
	// If we see a digit then consume as a number.
	switch ch := s.ch; { // Read the next rune.
	case isLetter(ch):
		//s.unread()
		tok, lit = s.scanIdent()
	default: // Otherwise read the individual character.
		s.next()
		switch ch {
		case eof:
			tok = token.EOF
		case '*':
			tok = token.MUL
		case ',':
			tok = token.COMMA
		case '(':
			tok = token.LPAREN
		case ')':
			tok = token.RPAREN
		case ';':
			tok = token.SEMICOLON
		default:
			tok, lit = token.ILLEGAL, string(ch)
		}
		if lit == "" {
			lit = tok.String()
		}
	}
	//return ILLEGAL, string(ch)
	return //tok, lit
}

func (s *Scanner) skipWhitespace() {

	//ch := s.ch
	//if ch == eof {
	//	return
	//}
	for s.ch == ' ' || s.ch == '\t' || s.ch == '\n' {
		s.next()
	}
	//s.unread()
}

// scanIdent consumes the current rune and all contiguous ident runes.
func (s *Scanner) scanIdent() (tok token.Token, lit string) {
	// Create a buffer and read the current character into it.
	offs := s.offset
	//buf.WriteRune(s.read())

	// Read every subsequent ident character into the buffer.
	// Non-ident characters and EOF will cause the loop to exit.
	/*for ch := s.ch; ch != eof && (isLetter(ch) || isDigit(ch) || ch == '_'); ch = s.read() {
		buf.WriteRune(ch)
	}
	s.unread()*/
	for isLetter(s.ch) || isDigit(s.ch) || s.ch == '_' {
		s.next()
	}
	lit = string(s.src[offs:s.offset])

	// If the string matches a keyword then return that keyword.
	switch strings.ToUpper(lit) {
	case "SELECT":
		tok = token.SELECT
	case "FROM":
		tok = token.FROM
	case "FUNCTION":
		tok = token.FUNCTION
	case "RETURN":
		tok = token.RETURN
	default:
		tok = token.IDENT
	}

	// Otherwise return as a regular identifier.
	return tok, lit
}

// read reads the next rune from the bufferred reader.
// Returns the rune(0) if an error occurs (or io.EOF is returned).
/*func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()

	if err != nil {
		return eof
	}
	return ch
}*/

func (s *Scanner) next() {
	if s.rdOffset < len(s.src) {
		s.offset = s.rdOffset
		if s.ch == '\n' {
			s.lineOffset = s.offset
		}
		r, w := rune(s.src[s.rdOffset]), 1
		switch {
		/*case r == 0:
		s.error(s.offset, "illegal character NUL")*/
		case r >= 0x80:
			// not ASCII
			r, w = utf8.DecodeRune(s.src[s.rdOffset:])
			/*if r == utf8.RuneError && w == 1 {
				s.error(s.offset, "illegal UTF-8 encoding")
			} else if r == bom && s.offset > 0 {
				s.error(s.offset, "illegal byte order mark")
			}*/
		}
		s.rdOffset += w
		s.ch = r
	} else {
		s.offset = len(s.src)
		if s.ch == '\n' {
			s.lineOffset = s.offset
		}
		s.ch = eof // eof
	}

}

// unread places the previously read rune back on the reader.
//func (s *Scanner) unread() { _ = s.r.UnreadRune() }

// isLetter returns true if the rune is a letter.
func isLetter(ch rune) bool { return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') }

// isDigit returns true if the rune is a digit.
func isDigit(ch rune) bool { return (ch >= '0' && ch <= '9') }
