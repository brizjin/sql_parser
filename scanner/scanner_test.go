package scanner

import (
	//"reflect"
	//"strings"
	"testing"
	//"bytes"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	//"sql_parser"
	//"sql_parser/ast"
	"sql_parser/token"
)

func TestScanner_Scan(t *testing.T) {

	Convey("Лексический разбор", t, func() {

		var tests = []struct {
			s   string
			tok token.Token
			lit string
		}{
			// Special tokens (EOF, ILLEGAL, WS)
			{``, token.EOF, "EOF"},
			{`#`, token.ILLEGAL, `#`},
			{",", token.COMMA, ","},
			{"(", token.LPAREN, "("},
			{")", token.RPAREN, ")"},
			{"FUNCTION", token.FUNCTION, "FUNCTION"},
			{";", token.SEMICOLON, ";"},

			// Misc characters
			{`*`, token.MUL, "*"},

			// Identifiers
			{`foo`, token.IDENT, `foo`},
			{`Zx12_3U_-`, token.IDENT, `Zx12_3U_`},

			// Keywords
			{`FROM`, token.FROM, "FROM"},
			{`SELECT`, token.SELECT, "SELECT"},
			{"SELECT    \t\n", token.SELECT, "SELECT"},
		}

		for i, tt := range tests {
			//s := NewScanner(strings.NewReader(tt.s))
			s := NewScanner([]byte(tt.s))
			_, tok, lit := s.Scan()

			Convey(fmt.Sprintf("Сравним получение токена из строки %d. %q ", i, tt.s), func() {

				Convey(fmt.Sprintf("Сравним тип токена %d=%d", tok, tt.tok), func() {
					So(tok, ShouldEqual, tt.tok)
					So(tok.String(), ShouldEqual, tok.String())
				})
				Convey(fmt.Sprintf("Сравним значение строки токена %q=%q", lit, tt.lit), func() {
					So(lit, ShouldEqual, tt.lit)
				})

			})
		}
		type T struct {
			tok token.Token
			lit string
		}

		tests2 := []struct {
			s    string
			toks []T
		}{
			{s: "SELECT * FROM table", toks: []T{
				{token.SELECT, "SELECT"},
				{token.MUL, "*"},
				{token.FROM, "FROM"},
				{token.IDENT, "table"},
			}},
			{s: "SELECT a,b FROM table", toks: []T{
				{token.SELECT, "SELECT"},
				{token.IDENT, "a"},
				{token.COMMA, ","},
				{token.IDENT, "b"},
				{token.FROM, "FROM"},
				{token.IDENT, "table"},
			}},
			{"function test(a varchar2)return clob;", []T{
				{token.FUNCTION, "function"},
				{token.IDENT, "test"},
				{token.LPAREN, "("},
				{token.IDENT, "a"},
				{token.IDENT, "varchar2"},
				{token.RPAREN, ")"},
				{token.RETURN, "return"},
				{token.IDENT, "clob"},
				{token.SEMICOLON, ";"},
			}},
		}

		for i, tt := range tests2 {
			Convey(fmt.Sprintf("Проверим разбор на лексемы строку %d,%q", i, tt.s), func() {
				s := NewScanner([]byte(tt.s))
				//for tok, lit := s.Scan(); tok != EOF; tok, lit = s.Scan() {
				for j := range tt.toks {
					_, tok, lit := s.Scan()
					Convey(fmt.Sprintf("Соответствие токенов %d tok(%d,%q)=tok(%d,%q)", j, tok, lit, tt.toks[j].tok, tt.toks[j].lit), func() {
						So(tok, ShouldEqual, tt.toks[j].tok)
						So(lit, ShouldEqual, tt.toks[j].lit)
					})
				}
			})
		}
	})
}
