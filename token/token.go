package token

import "strconv"

type Token int

const (
	// Special tokens
	ILLEGAL Token = iota
	EOF
	//WS

	//////////////////////////
	literal_beg //
	// Identifiers and basic type literals
	// (these tokens stand for classes of literals)
	IDENT  // main
	INT    // 12345
	FLOAT  // 123.45
	STRING // 'abc'
	literal_end
	////////////////////////////////

	////////////////////////////////
	operator_beg
	// Operators and delimiters
	ADD // +
	SUB // -
	MUL // *
	DIV // /
	//REM // %

	//EQL    // ==
	LSS    // <
	GTR    // >
	ASSIGN // =
	//NOT    // !

	NEQ    // !=
	LEQ    // <=
	GEQ    // >=
	DEFINE // :=

	LPAREN // (
	LBRACK // [
	LBRACE // {
	COMMA  // ,
	PERIOD // .

	RPAREN    // )
	RBRACK    // ]
	RBRACE    // }
	SEMICOLON // ;
	COLON     // :
	operator_end
	//////////////////////////

	/////////////////////////
	keyword_beg
	// Keywords
	SELECT
	FROM
	FUNCTION
	RETURN
	keyword_end
	/////////////////////////
)

var tokens = [...]string{
	ILLEGAL: "ILLIGAL",
	EOF:     "EOF",
	//WS:      "WS",

	IDENT: "IDENT",
	INT:   "INT",
	FLOAT: "FLOAT",
	//IMAG:   	"IMAG",
	//CHAR:   	"CHAR",
	STRING: "STRING",

	///////////////////////
	ADD: "+",
	SUB: "-",
	MUL: "*",
	DIV: "/",
	//REM: "%",

	//EQL:    "==",
	LSS:    "<",
	GTR:    ">",
	ASSIGN: "=",
	//NOT:    "!",

	NEQ:    "!=",
	LEQ:    "<=",
	GEQ:    ">=",
	DEFINE: ":=",

	LPAREN: "(",
	LBRACK: "[",
	LBRACE: "{",
	COMMA:  ",",
	PERIOD: ".",

	RPAREN:    ")",
	RBRACK:    "]",
	RBRACE:    "}",
	SEMICOLON: ";",
	COLON:     ":",
	//////////////////////

	// Keywords
	SELECT:   "SELECT",
	FROM:     "FROM",
	FUNCTION: "FUNCTION",
	RETURN:   "RETURN",
}

func (tok Token) String() string {
	s := ""
	if 0 <= tok && tok < Token(len(tokens)) {
		s = tokens[tok]
	}
	if s == "" {
		s = "token(" + strconv.Itoa(int(tok)) + ")"
	}
	return s
}
